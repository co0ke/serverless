﻿namespace AwsDotnetCsharp
{
    public class ResponseBody
    {
        public string Message { get; set; }
        public RequestBody Request { get; set; }
    }
}
