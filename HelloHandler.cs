using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using System.Collections.Generic;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AwsDotnetCsharp
{
    public class HelloHandler
    {
        // TODO
        // make method async
        // https://www.stevejgordon.co.uk/publishing-to-aws-simple-notification-service-sns-from-asp-net-core

        public APIGatewayProxyResponse Hello(APIGatewayProxyRequest request)
        {
            LambdaLogger.Log($"request: {Newtonsoft.Json.JsonConvert.SerializeObject(request)}");

            var requestBody = Newtonsoft.Json.JsonConvert.DeserializeObject<RequestBody>(request.Body);
            LambdaLogger.Log($"request body content value: {requestBody.Content}");

            var topicArn = System.Environment.GetEnvironmentVariable("topicArn");
            LambdaLogger.Log($"topicArn: {topicArn}");

            var snsClient = new AmazonSimpleNotificationServiceClient();

            var message = Newtonsoft.Json.JsonConvert.SerializeObject(requestBody);
            var publishRequest = new PublishRequest(topicArn, message, "test subject from lambda");
            var publishResponse = snsClient.PublishAsync(publishRequest).Result;

            var responseBody = new ResponseBody
            {
                Message = $"Your function executed successfully! Message ID: {publishResponse.MessageId}",
                Request = requestBody,
            };

            return new APIGatewayProxyResponse
            {
                IsBase64Encoded = false,
                StatusCode = 200,
                Body = Newtonsoft.Json.JsonConvert.SerializeObject(responseBody),
                Headers = new Dictionary<string, string>() { { "myheader", "myheadervalue" } }
            };
        }
    }
}