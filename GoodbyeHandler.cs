using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using System.Collections.Generic;

namespace AwsDotnetCsharp
{
    public class GoodbyeHandler
    {
        public APIGatewayProxyResponse Goodbye(SQSEvent sqsEvent)
        {
            LambdaLogger.Log($"Goodbye");

            LambdaLogger.Log($"Beginning to process {sqsEvent.Records.Count} records...");

            foreach (var record in sqsEvent.Records)
            {
                LambdaLogger.Log($"Message ID: {record.MessageId}");
                LambdaLogger.Log($"Event Source: {record.EventSource}");
                LambdaLogger.Log($"Record Body:");
                LambdaLogger.Log(record.Body);
            }

            //return $"Processed {sqsEvent.Records.Count} records.";

            var typedMessageBody = Newtonsoft.Json.JsonConvert.DeserializeObject<RequestBody>(sqsEvent.Records[0].Body);
            LambdaLogger.Log($"typedMessageBody: {typedMessageBody}.");

            LambdaLogger.Log("Processing complete.");
            
            return new APIGatewayProxyResponse
            {
                IsBase64Encoded = false,
                StatusCode = 200,
                Body = Newtonsoft.Json.JsonConvert.SerializeObject(typedMessageBody),
                Headers = new Dictionary<string, string>() { { "myheader", "myheadervalue" } }
            };
        }
    }
}